import { convert } from "./Main";
import { join, dirname } from "path";
import { JsonReader } from "./convert/reader/JsonReader";
import { pathData } from "./utils/PathUtil";

pathData.root = __dirname;
// let path = join('E:/work/unity/fudao/table/tablec.json');
let path = join(__dirname,'../test/table2/tablec.json');
// let path = 'F:/work/gitee/gamebattle/timelinelog/docs/table/tablec.json';
let headPath = dirname(path);
new JsonReader().read(path, null, (res) => {
    convert(res, headPath);
});
