import { convert } from "./convert/Convert";
import { ConvertMgr } from "./convert/mgr/ConvertMgr";
import { ErrLogMgr } from "./convert/mgr/ErrLogMgr";
import { LanguageMgr } from "./convert/mgr/LanguageMgr";
import { CodeMgr } from "./convert/mgr/CodeMgr";
import { IPlugIn, ITableConfig, ITableData, ITableJson } from "./inters/InterfaceApi";


let errlogMgr = ErrLogMgr.inst;
let tsMgr = CodeMgr.inst;
let convertMgr = ConvertMgr.inst;
let langMgr = LanguageMgr.inst;

export { convert, errlogMgr, tsMgr, langMgr, convertMgr, ITableConfig,ITableJson,ITableData,IPlugIn };

