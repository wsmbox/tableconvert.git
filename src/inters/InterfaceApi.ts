import { Convert } from "../convert/Convert";
import { IKeyVo } from "../convert/utils/KeyVoUtil";

export interface IFileMerge {
    /** 需要合并的文件列表,为空则在其他的列表不存在的情况下默认选则所有的文件 */
    files?: string[];
    /** 合并是否压缩: 0为直接合并为一个文件; 1为合并文件后进行zip压缩 */
    compress: number;
    /** 合并之后的文件路径 */
    path: string;
    /** 合并之后的文件名，如果有平台区分，则会形成xxx_tt的形式。如果有lang和平台同时存在，变成xx_tt_cn形式 */
    name?: string;
    /** 合并之后的文件扩展名，例如: ".bin", ".config" */
    nameExt?: string;
    /** 分支拆分模式；0为所有文件都合并为一个文件(xx.bin)；
     * 1为将所有分支都分离成各自的不重名表文件(xx_tt.bin, xx_tt_en.bin) */
    branchSplit?: number;
}
export interface ITableOutput {
    /** 导出的代码类型，js; json; d.ts; ... */
    type: string;
    /** 当前导出的对应type的文件路径 */
    path?: string | string[];
    /** 数据拆分（为了减少导出数据量）: 0为不拆分; 1为title和数据拆分; ... */
    dataSplit?: number;
    /** 当前导出的文件需要合成设置，为空则不合并 */
    merge?: IFileMerge[];
    /** 如果是js导出,这里定义的是注册到window上的代码 */
    windowRegister?: string;
    /** 如果是ts，或者dts，这里按照配置名字来导出，默认是sdata */
    fileName?: string;
    /** 如果需要包裹文件内容，这里用来包裹的头，包含"{", eg: namespace xxx{ */
    fileStart?: string;
    /** 如果需要包裹文件内容，这里用来包裹的尾，一般只有包含"}", eg: } */
    fileEnd?: string;
}

export interface ITableConfig {
    /** 导入配置 */
    input?: {
        /** 导入表的目录,不填则是使用和当前配置同级目录 */
        path?: string;
        /** 导出时，指定前缀的表名才能导出，和 filterPrefix 互斥; eg: 可以将开头为sh的tab都导出 */
        exportPrefix: string;
    };
    /** 导出配置 */
    output: ITableOutput[];
    /** 如果是dts的导出，这里定义导出的配置 */
    tsOption?: {
        /** 导出的是表 前缀这里加上 */
        tablePrefix: string;
        /** 导出的是类型 前缀这里加上 */
        typePrefix: string;
        /** 导出的是jsoncls 是否把JsonCls字符替换 */
        jsonClsReplace: string;
    }
    /** 如果有language，这里设置font的导出目录 */
    fontPath?: string;
    /** 错误日志设置 */
    errorLog?: {
        /** 是否开启数据查错功能（内置插件） */
        check: boolean;
        /** 错误日志输出路径 */
        outputPath: string;
    };
    /** 插件设置，为空则不使用外部插件 */
    plugin?: {
        /** 外部插件的主目录 */
        path: string
    }
}

/** 每张表生成的基础对应数据，不适合const和lang */
export interface ITableJson {
    primaryKey: string;
    langKeys: string[];
    list: any[]
}

/** 从xlsx中读取出来的表格数据（原始数据） */
export interface ITableObj {
    fileName: string;
    tabName: string;
    data: string[][];
    platName: string;
}

/**
 * 存储被表格转换处理之后的数据
 */
export interface ITableData {
    type: string;// 用来表示是默认base的数据，或者是const，或者是lang
    keys: IKeyVo[];
    tableName: string;
    /** 这里有可能是ITableJson，或者是Const的Obj，或者是Language的Obj */
    data: any;
    /** 转表中错误的报错内容 */
    errLog?: string[];
    /** 如果有平台信息 */
    plat?: string;
}

/** 读取接口 */
export interface IReader {
    read(path: string, options?: any, callback?: (res?: any) => void): any;
}

/** 生成目标文件接口，options中设置参数 */
export interface IWriter {
    write(data: any, options?: any, callback?: (res?: any) => void): any;
}

/** 处理中间数据的插件接口 */
export interface IPlugIn {
    start(data: any, options?: any, callback?: (res?: any) => void): any;
}

export interface IConvertParser {
    getData(key: string): any;
    //格式化数据
    parseValue();
    order: number;
}

export interface ICustomPlus {
    OnStart(convert: Convert);
    OnComplete(convert: Convert);
}

export interface ITypeParser {
    parseValue(value: string): any;
    typeName: string;
    keyVo: IKeyVo;
}
