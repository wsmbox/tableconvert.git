
export function hmsFormat(timeStamp: number) {
    let nd = new Date(timeStamp);

    let hh = nd.getHours() + '';
    let mm = nd.getMinutes() + '';
    let ss = nd.getSeconds() + '';
    hh = hh.length > 1 ? hh : '0' + hh;
    mm = mm.length > 1 ? mm : '0' + mm;
    ss = ss.length > 1 ? ss : '0' + ss;
    return `${hh}:${mm}:${ss}`;
}
export function logout(...msg) {
    console.log.apply(null, [`[${hmsFormat(Date.now())}]`, ...msg]);
}
export function logWarn(...msg) {
    console.warn.apply(null, [`[${hmsFormat(Date.now())}]`, ...msg]);
}