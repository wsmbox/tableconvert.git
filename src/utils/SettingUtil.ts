import { join } from "path";
import { JsonWriter } from "../convert/writer/JsonWriter";
import { ITableConfig } from "../Main";

export class SettingUtil {

    static createTablecJson(path: string) {
        let data: ITableConfig = {
            output: [
                {
                    type: "json",
                    merge: [
                        {
                            compress: 1,
                            path: '../output/json/allinone'
                        }
                    ]
                }, {
                    type: "ts",
                    path: "../output/ts"
                }
            ]
        }
        new JsonWriter().write(data, { path: join(path, 'tablec.json'), jsonFormat: true })
    }
}