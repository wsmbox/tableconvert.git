import { isAbsolute, join } from "path";

/** 获取绝对路径 */
export function getAbsolutePath(str: string, rootDir?: string) {
    if (!str) return ''
    if (isAbsolute(str)) {
        // 绝对路径
        return str;
    } else {
        if (rootDir) {
            return join(rootDir, str);
        } else {
            return join(process.cwd(), str);
        }
    }
}

export var pathData = {
    root: ""
}