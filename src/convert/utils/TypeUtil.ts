import { typeParserMgr } from "../mgr/TypeParserMgr";

export class TypeUtil {
    static getType(type: string = "string") {
        switch (type.toLowerCase()) {
            case "number":
                return "number";
            case "int":
                return "int";
            case "float":
                return "float";
            case "array<string>":
            case "string[]":
                return "string[]";
            case "array<number>":
            case "number[]":
                return "number[]";
            case "float[]":
                return "float[]";
            case "int[]":
                return "int[]";
            case "array<enum>":
            case "enum[]":
                return "number[]";
            case "enum":
                return "number";
            case "json":
                return "any";
            case "tags":
                return "number";
            case "boolean":
                return "boolean";
            case "bool":
                return "bool";
            case "string":
                return "string";
            default:
                let parser = typeParserMgr.getParser(type);
                if (parser != null) {
                    if (type.indexOf("[]") > 0) {
                        return parser.typeName + "[]";
                    } else {
                        return parser.typeName;
                    }
                }
                return "string";
        }
    }
}