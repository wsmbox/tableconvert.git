import { IEnum, ITags, IConst, IJson } from "./ClsFactory";
import { Pinyin } from "../../pinyin/Pinyin";
import { convertMgr } from "../mgr/ConvertMgr";
import { table } from "console";

export interface IKeyVo {
    name: string;
    type: string;
    primaryKey: boolean;
    content?: any;
    des?: string;
    enum?: IEnum;
    tags?: ITags;
    json?: IJson;
    enumkey?: IConst;
}

export class KeyVoUtil {

    static parserKey(keyStr: string, tableName: string) {
        var arr = keyStr.replace(/\r\n/g, "\n").split("\n");
        var key = arr[0].split(":")[0];
        var type = arr[0].split(":")[1];
        var keyVo: IKeyVo = { type: type || "string", name: key, primaryKey: false, content: {} };
        for (var i = 1; i < arr.length; i++) {
            var keyArr = arr[i].split(":");
            keyVo.content[keyArr.shift()] = keyArr.join(":");
        }
        if (keyVo.content.primaryKey === "true") {
            keyVo.primaryKey = true;
        }
        if (!type) {
            console.error(tableName + "表头错误：" + keyStr);
        }
        type = (type || keyVo.type).toLocaleLowerCase();
        // todo 还有enum，tags等其他的类型需要处理
        if (type === "enum") {
            let numStr: string = keyVo.content.enum || "";
            let enumSplit = "|";
            if (numStr.indexOf("|") < 0 && numStr.indexOf(",") >= 0) {
                enumSplit = ",";
            }
            let enumArr = numStr.split(enumSplit);
            let tp = convertMgr.crtConvert.convertCfg.tsOption.typePrefix;
            let typeName = tp + this._getFirstCapWord(type);
            let enumVo: IEnum = new IEnum(typeName + tableName + keyVo.name);
            if (enumArr.length > 0) {
                for (let i = 0; i < enumArr.length - 1; i += 2) {
                    let pyKey = Pinyin.Inst.getFullChars(enumArr[i]);
                    enumVo.addVar(pyKey, parseInt(enumArr[i + 1]), enumArr[i]);
                }
            }
            keyVo.enum = enumVo;
        } else if (type === "array<enum>" || type === "enum[]") {

            let numStr = keyVo.content.enum || "";
            let enumSplit = "|";
            if (numStr.indexOf("|") < 0 && numStr.indexOf(",") >= 0) {
                enumSplit = ",";
            }
            let enumArr = numStr.split(enumSplit);
            let tp = convertMgr.crtConvert.convertCfg.tsOption.typePrefix;

            let enumVo: IEnum = new IEnum(tp + "Enum" + tableName + keyVo.name);
            if (enumArr.length > 0) {
                for (let i = 0; i < enumArr.length - 1; i += 2) {
                    let pyKey = Pinyin.Inst.getFullChars(enumArr[i]);
                    enumVo.addVar(pyKey, parseInt(enumArr[i + 1], 10), enumArr[i]);
                }
            }
            keyVo.enum = enumVo;
        } else if (type === "tags") {
            let numStr = keyVo.content.tags || "";
            let tagsArr = numStr.split("|");
            let tp = convertMgr.crtConvert.convertCfg.tsOption.typePrefix;
            let typeName = tp + this._getFirstCapWord(type);
            let tagsVo: ITags = new ITags(typeName + tableName + keyVo.name);
            if (tagsArr.length > 0) {
                for (let i = 0; i < tagsArr.length - 1; i += 2) {
                    let pyKey = Pinyin.Inst.getFullChars(tagsArr[i]);
                    tagsVo.addVar(pyKey, parseInt(tagsArr[i + 1]), tagsArr[i]);
                }
            }
            keyVo.tags = tagsVo;
        } else if (type == "enumkey") {
            let tp = convertMgr.crtConvert.convertCfg.tsOption.typePrefix;
            let typeName = tp + this._getFirstCapWord('enum');
            let enumVo: IConst = new IConst(typeName + tableName + "ID");
            keyVo.enumkey = enumVo;
        }
        return keyVo;
    }

    private static _getFirstCapWord(word: string) {
        return word.substr(0, 1).toUpperCase() + word.substr(1);
    }

    /**拼接主键 */
    static getRealKey(d: any, primaryKeys: string[]) {
        var realKey = d[primaryKeys[0]];
        for (var i = 1; i < primaryKeys.length; i++) {
            realKey += "_" + d[primaryKeys[i]];
        }
        return realKey;
    }
}