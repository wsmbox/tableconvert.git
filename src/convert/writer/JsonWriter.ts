import { existsSync, writeFileSync } from 'fs';
import { dirname } from "path";
import { IWriter } from '../../inters/InterfaceApi';
import { mkdirsSync } from '../../utils/CpFile';

export class JsonWriter implements IWriter {

    write(data: any, options?: any, callback?: (res?: any) => void) {
        options = options || {}
        let path = options.path;// 这里的path要有具体到文件名全路径：F:/xx/test.json
        let dir = dirname(path);
        if (!existsSync(dir)) {
            mkdirsSync(dir);
        }
        let str = ''
        if (options.jsonFormat) {
            str = JSON.stringify(data, null, '    ');
        } else {
            str = JSON.stringify(data);
        }
        writeFileSync(path, str)
        if (!!callback) callback();
    }

}