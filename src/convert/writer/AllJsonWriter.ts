import { existsSync, writeFileSync } from 'fs';
import { join } from "path";
import { ITableOutput, IWriter } from '../../inters/InterfaceApi';
import { mkdirsSync } from '../../utils/CpFile';
import { convertMgr } from '../mgr/ConvertMgr';

export class AllJsonWriter implements IWriter {

    private _cfg: ITableOutput;

    write(data: any, options?: any, callback?: (res?: any) => void) {
        this._cfg = data;
        let opPath: string[]
        if (typeof this._cfg.path == 'string') {
            opPath = [];
        } else {
            opPath = this._cfg.path
        }
        // let path = options.path;
        opPath.forEach(p => {

            let dir = p;//join(path, p);
            if (!existsSync(dir)) {
                mkdirsSync(dir);
            }

            let convert = convertMgr.crtConvert;
            let tableData = convert.tableData;

            for (let table in tableData) {
                let td = tableData[table];
                // 先将所有的json都导出
                let str = JSON.stringify(td.data);

                writeFileSync(join(dir, `${table}`), str);
            }
        })

        if (!!callback) callback();
    }

}