import { existsSync, writeFileSync } from 'fs';
import { join } from "path";
import { IWriter } from "../../inters/InterfaceApi";
import { mkdirsSync } from '../../utils/CpFile';
import { Convert } from "../Convert";
import { convertMgr } from "../mgr/ConvertMgr";
import { langMgr } from "../mgr/LanguageMgr";
export class FontWriter implements IWriter {

    convert: Convert;

    write(data: any, options?: any, callback?: (res?: any) => void) {

        this.convert = convertMgr.crtConvert;
        if(!this.convert.convertCfg.fontPath){
            return;
        }

        for (let fontName in langMgr.fontHash) {
            let font = langMgr.fontHash[fontName]
            for (let lang in font) {

                let dir = join(this.convert.convertCfg.fontPath, lang);
                if (!existsSync(dir)) {
                    mkdirsSync(dir);
                }

                let str = font[lang];
                let url = join(dir, fontName + '.txt')
                writeFileSync(url, str)
            }
        }

        if (!!callback) callback();
    }

}