import { existsSync, writeFileSync } from 'fs';
import { join } from "path";
import { IWriter } from "../../inters/InterfaceApi";
import { mkdirsSync } from '../../utils/CpFile';
import { errLogMgr } from "../mgr/ErrLogMgr";
import { convertMgr } from '../mgr/ConvertMgr';

export class ErrLogWriter implements IWriter {


    write(data: any, options?: any, callback?: (res?: any) => void) {

        if (!errLogMgr.errLog.length) {
            return
        }

        let convert = convertMgr.crtConvert;

        let dir = convert.convertCfg.errorLog.outputPath;
        if (!dir) return;
        if (!existsSync(dir)) {
            mkdirsSync(dir);
        }

        let txt = '';
        errLogMgr.errLog.forEach(el => {
            txt += (el + "\r\n");
        })
        writeFileSync(join(dir, `err.log`), txt);

        if (!!callback) callback();
    }

}