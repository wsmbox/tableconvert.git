import { IReader } from "../../inters/InterfaceApi";

const NodeXlsx = require("node-xlsx")

export class XlsxReader implements IReader {

    read(path: string, options?: any, callback?: ((res?: any) => void) | undefined): any {
        let data = NodeXlsx.parse(path, options);
        if (!!callback) {
            callback(data);
        } else {
            return data;
        }
    }

}