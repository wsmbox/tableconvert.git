import { readFileSync } from "fs";
import { IReader } from "../../inters/InterfaceApi";

export class JsonReader implements IReader {

    read(path: string, options?: any, callback?: ((res?: any) => void) | undefined): any {
        let jsonData = JSON.parse(readFileSync(path).toString())
        if (!!callback) {
            callback(jsonData);
        } else {
            return jsonData;
        }
    }

}