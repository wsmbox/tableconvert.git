import { ITypeParser } from "../../inters/InterfaceApi";

export class TypeParserMgr{
    private static _inst: TypeParserMgr;
    static get inst(): TypeParserMgr {
        TypeParserMgr._inst = TypeParserMgr._inst || new TypeParserMgr();
        return TypeParserMgr._inst;
    }

    private _hash:any;
    constructor(){

    }

    public init(){
        this._hash={};
    }

    public registParser(name:string,parser:ITypeParser){
        this._hash[name.toLocaleLowerCase()]=parser;
    }

    public getParser(name:string):ITypeParser{
        return this._hash[name.toLocaleLowerCase()];
    }
}

export var typeParserMgr=TypeParserMgr.inst;