import { Convert } from "../Convert";

/**
 * 此类是用来处理以后如果有多个同时处理的情况下用，暂且只用crt
 * 多个的时候，要加上sid，来区分每个不同的convert的流转情况
 */
export class ConvertMgr {
    private static _inst: ConvertMgr;
    static get inst(): ConvertMgr {
        ConvertMgr._inst = ConvertMgr._inst || new ConvertMgr();
        return ConvertMgr._inst;
    }

    crtConvert: Convert;
}

export var convertMgr = ConvertMgr.inst;
