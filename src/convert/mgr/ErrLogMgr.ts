export class ErrLogMgr {
    private static _inst: ErrLogMgr;
    static get inst(): ErrLogMgr {
        ErrLogMgr._inst = ErrLogMgr._inst || new ErrLogMgr();
        return ErrLogMgr._inst;
    }

    /** 转换时需要生成的描述文件 */
    errLog: string[]

    constructor() {

        this.init()
    }

    init(){
        this.errLog = []
    }

    addLog(logs: string[]) {
        if (!logs || !logs.length) {
            return
        };
        this.errLog = this.errLog.concat(logs);
    }
}

export var errLogMgr = ErrLogMgr.inst;
