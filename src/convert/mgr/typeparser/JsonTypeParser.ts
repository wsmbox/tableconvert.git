import { ITypeParser } from "../../../inters/InterfaceApi";
import { IKeyVo, KeyVoUtil } from "../../utils/KeyVoUtil";
import { ValueUtil } from "../../utils/ValueUtil";
import { typeParserMgr } from "../TypeParserMgr";

export class JsonTypeParser implements ITypeParser {
    private _name: string;
    private _keyvo: IKeyVo;
    constructor(name: string, keyVo: IKeyVo) {
        this._name = name;
        this._keyvo = keyVo;
    }

    parseValue(value: string): any {
        return JSON.parse(value);
    }
    get typeName(): string {
        return this._keyvo.json ? this._keyvo.json.name : this._name;
    }
    get keyVo(): IKeyVo {
        return this._keyvo;
    }
}
export class JsonClsTypeParser implements ITypeParser {
    private _name: string;
    private _keyvo: IKeyVo;
    constructor(name: string, keyVo: IKeyVo) {
        this._name = name;
        this._keyvo = keyVo;
    }
    get keyVo(): IKeyVo {
        return this._keyvo;
    }

    parseValue(value: string): any {
        // 传入的值，可能的情况： [id:"a",num:1,rate:20|id:"b",num:2,rate:40]
        let arr = [];
        value = value.trim();
        let sidx = value.indexOf("[");
        let eidx = value.lastIndexOf("]");
        if (sidx == 0 && eidx == value.length - 1) {
            // 如果开始的第一个字符就是 数组 的开头 [ 符号
            let temp = value.substr(1, value.length - 2);
            let str = temp.split("|");
            for (let i = 0; i < str.length; i++) {
                arr.push(this._parse(str[i]));
            }
            return arr;
        }
        return this._parse(value);
    }
    private _parse(value: string): any {
        let kvs = value.split(",");
        let obj = {};
        for (let i = 0; i < kvs.length; i++) {
            let kv = kvs[i].split(":");
            let kv0 = kv[0].trim();
            let kv1 = kv[1].trim();
            let type = this._keyvo.content[kv0];
            let v = this._parseValue(type, kv1);
            obj[kv0] = v;
        }
        return obj;
    }
    private _parseValue(type: string, value: string) {
        var result: any;
        switch (type.toLowerCase()) {
            case "number":
            case "float":
                result = this.toNum(value);
                break;
            case "int":
                result = this.toInt(value);
                break;
            case "array<string>":
            case "string[]":
                result = this.toArray(value, "string");
                break;
            case "array<number>":
            case "number[]":
            case "float[]":
                result = this.toArray(value, "number");
                break;
            case "int[]":
                result = this.toArray(value, "int");
                break;
            case "string":
                result = value.toString();
                break;
            default:
                let parser = typeParserMgr.getParser(type);
                if (parser != null && parser.keyVo != null && parser.keyVo.enum != null) {
                    // 这个是枚举类型，直接toInt
                    result = this.toInt(value);
                } else {
                    result = value.toString();
                }
                break;
        }
        return result;
    }

    private toArray(value: string, child: "string" | "number" | "int" | "float"): Array<number | string> {
        var result: Array<number | string>;
        if (value == null || value === undefined || value === "") return [];

        result = value.toString().replace(/\r\n/g, "\n").replace(/,/g, "\n").split("\n");
        for (var i = 0; i < result.length; i++) {
            if (result[i] === "") {
                result.splice(i, 1);
                i--;
                continue;
            }
            if (child == "number" || child == "float") {
                result[i] = this.toNum(<string>result[i]);
            } else if (child == "int") {
                result[i] = this.toInt(<string>result[i]);
            }
        }
        return result;
    }
    private toNum(str: string): number {
        var num = parseFloat(str);
        if (isNaN(num)) return 0;
        return num;
    }
    private toInt(str: string): number {
        var num = parseInt(str);
        if (isNaN(num)) return 0;
        return num;
    }
    get typeName(): string {
        return this._keyvo.json ? this._keyvo.json.name : this._name;
    }
}