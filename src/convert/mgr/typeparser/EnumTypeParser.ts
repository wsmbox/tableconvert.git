import { ITypeParser } from "../../../inters/InterfaceApi";
import { IKeyVo, KeyVoUtil } from "../../utils/KeyVoUtil";
import { ValueUtil } from "../../utils/ValueUtil";

export class EnumTypeParser implements ITypeParser {
    private _name: string;
    private _keyvo: IKeyVo;
    constructor(name: string, keyVo: IKeyVo) {
        this._name = name;
        this._keyvo = keyVo;
    }

    parseValue(value: string): any {
        return ValueUtil.parserValue(value, this._keyvo);
    }
    get typeName(): string {
        return this._keyvo.enum ? this._keyvo.enum.name : this._keyvo.tags.name;
    }
    get keyVo(): IKeyVo {
        return this._keyvo;
    }
}