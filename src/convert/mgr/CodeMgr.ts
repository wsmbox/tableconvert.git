import { IInterface, ICls } from "../utils/ClsFactory";

export class CodeMgr {
    private static _inst: CodeMgr;
    static get inst(): CodeMgr {
        CodeMgr._inst = CodeMgr._inst || new CodeMgr();
        return CodeMgr._inst;
    }

    /** 转换时需要生成的描述文件 */
    dtsList: ICls[]

    constructor() {

        this.init()
    }

    init(){
        this.dtsList = []
    }

    addCls(clsList: ICls[]) {
        this.dtsList = this.dtsList.concat(clsList);
    }
}

export var codeMgr = CodeMgr.inst;
