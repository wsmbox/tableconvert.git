import { IConvertParser, IPlugIn, ITableData, ITableObj } from "../../inters/InterfaceApi";
import { Convert } from "../Convert";
import { codeMgr } from "../mgr/CodeMgr";
import { convertMgr } from "../mgr/ConvertMgr";
import { errLogMgr } from "../mgr/ErrLogMgr";
import { EnumTypeParser } from "../mgr/typeparser/EnumTypeParser";
import { typeParserMgr } from "../mgr/TypeParserMgr";
import { ICls } from "../utils/ClsFactory";
import { HashMap } from "../utils/HashMap";
import { IKeyVo, KeyVoUtil } from "../utils/KeyVoUtil";
import { ValueUtil } from "../utils/ValueUtil";

export class EnumParser implements IPlugIn,IConvertParser{
    private _errLogs: string[];
    protected _hash: HashMap<any>;
    protected _clsList: Array<ICls>;
    private _data:ITableObj;
    convert: Convert;

    start(data: any, options?: any, callback?: (res?: any) => void) {
        this._data=data;
        return this;
    }

    public parseValue(){
        this.convert = convertMgr.crtConvert;
        let tableData: ITableObj = this._data;
        let keys = tableData.data[0];
        let descs = tableData.data[1];
        this._clsList = [];
        let keyVos: IKeyVo[] = []
        keys.forEach((keyStr, idx) => {
            let keyVo = KeyVoUtil.parserKey(keyStr, tableData.fileName);
            keyVo.des = descs[idx];

            keyVos.push(keyVo)
        })

        let values=this._data.data.slice(2);
        let tp = this.convert.convertCfg.tsOption.tablePrefix;
        let realyKeys:IKeyVo[]=[];
        this._hash = new HashMap();
        for (let i = 0; i < values.length; i++) {
            var propList = values[i];
            if (propList.length == 0) continue;
            var key: string = "";
            var item: any = {};
            for (let j = 0; j < propList.length; j++) {
                let keyVo = keyVos[j];
                try {
                    var value = ValueUtil.parserValue(propList[j], keyVo);
                } catch (e) {
                    this._errLogs.push(this._data.fileName + ":" + propList[0] + "---" + keyVo.name + "--数据错误:" + e.message);
                }
                if (keyVo.primaryKey) {
                    key = value;
                }
                item[keyVo.name] = value;
            }
            if (key === "") {
                key = i.toString();
            }
            var vKeyVo: IKeyVo;
            let type=item.type.toLocaleLowerCase();
            if (type === "array<enum>" || type === "enum[]" || type=="enum") {
                vKeyVo = KeyVoUtil.parserKey(key + ":" + item.type + "\nenum:" + item.content, "");
                vKeyVo.enum.setDes(item.des);
                this._clsList.push(vKeyVo.enum);
            } else if(type == "tags"){
                vKeyVo = KeyVoUtil.parserKey(key + ":" + item.type + "\ntags:" + item.content, "");
                vKeyVo.tags.setDes(item.des);
                this._clsList.push(vKeyVo.tags);
            }
            if(vKeyVo!=null){
                typeParserMgr.registParser(key,new EnumTypeParser(key,vKeyVo));
                realyKeys.push(vKeyVo);
            }
        }
        errLogMgr.addLog(this._errLogs)
        codeMgr.addCls(this._clsList);

        let tempTableData: ITableData = {
            type: "enum",
            keys: realyKeys,
            tableName: tableData.fileName,
            data:null,
            errLog: this._errLogs,
            plat: tableData.platName
        }
        this.convert.addTableData(tempTableData, tableData.fileName, tableData.platName);
    }

    public getData(){return null;}

    get order(){return 100;}
}