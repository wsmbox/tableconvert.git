import { IConvertParser, IPlugIn, ITableObj } from "../../inters/InterfaceApi";
import { Convert } from "../Convert";
import { convertMgr } from "../mgr/ConvertMgr";
import { errLogMgr } from "../mgr/ErrLogMgr";
import { langMgr } from "../mgr/LanguageMgr";
import { codeMgr } from "../mgr/CodeMgr";
import { ICls, IInterface } from "../utils/ClsFactory";
import { HashMap } from "../utils/HashMap";
import { IKeyVo, KeyVoUtil } from "../utils/KeyVoUtil";
import { ValueUtil } from "../utils/ValueUtil";

export class LanguageParser implements IPlugIn, IConvertParser {

    private _errLogs: string[];
    protected _hash: HashMap<any>;
    protected _resultData: any;
    protected _clsList: Array<ICls>;
    convert: Convert;
    private _data:ITableObj;

    start(data: any, options?: any, callback?: (res?: any) => void) {
        this._data=data;
        return this;
    }

    parseValue(){
        this.convert = convertMgr.crtConvert;
        let tableData: ITableObj = this._data;
        let keys = tableData.data[0];
        let descs = tableData.data[1];
        let values = tableData.data.slice(2);
        let keyVos: IKeyVo[] = []
        this._clsList = [];
        let tp = this.convert.convertCfg.tsOption.tablePrefix
        var iInter = new IInterface(tp + tableData.fileName);
        this._clsList.push(iInter);
        keys.forEach((keyStr, idx) => {
            let keyVo = KeyVoUtil.parserKey(keyStr, tableData.fileName);
            keyVo.des = descs[idx];

            keyVos.push(keyVo)
        })

        this._hash = new HashMap();
        this._resultData = {};
        for (let i = 0; i < values.length; i++) {
            var propList = values[i];
            if (propList.length == 0) continue;
            var key: string = "";
            var item: any = {};
            var lang: any = {};
            for (let j = 0; j < propList.length; j++) {
                var keyVo = keyVos[j];
                try {
                    var value = ValueUtil.parserValue(propList[j], keyVo);
                } catch (e) {
                    this._errLogs.push(tableData.fileName + ":" + propList[0] + "---" + keyVo.name + "--数据错误:" + e.message);
                }
                if (keyVo.primaryKey) {
                    key = value;
                }
                if (keyVo.content.lang) {
                    lang[keyVo.content.lang] = value;
                    langMgr.addLangList(keyVo.content.lang);
                    continue;
                }
                item[keyVo.name] = value;
            }
            if (key === "") {
                key = i.toString();
            }
            for (var langKey in lang) {
                langMgr.addLang(lang[langKey], key, langKey, item.font);
            }
            iInter.addVar(key, "string", lang["cn"]);
            this._hash.add(key, item);
        }

        langMgr.exportName = tableData.fileName

        errLogMgr.addLog(this._errLogs)
        codeMgr.addCls(this._clsList);
    }

    getData(key: string) {
        return null;
    }

    get order(){return 4;}
}