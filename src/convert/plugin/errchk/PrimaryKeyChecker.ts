import { IPlugIn, ITableJson } from "../../../inters/InterfaceApi";
import { logout } from "../../../utils/LogUtil";
import { convertMgr } from "../../mgr/ConvertMgr";
import { errLogMgr } from "../../mgr/ErrLogMgr";

/** 判断唯一key是否有重复 */
export class PrimaryKeyChecker implements IPlugIn {

    start(data: any, options?: any, callback?: (res?: any) => void) {
        let conv = convertMgr.crtConvert;
        for (let name in conv.tableData) {
            let td = conv.tableData[name];
            if (td.type != 'base') {
                continue;
            }
            let tjson: ITableJson = td.data;
            if(td.data==null)continue;
            let keys = tjson.primaryKey.split(',');// 如果有多个primarykey，要注意将数据中的数据相加来判重
            let chkHash = {};
            tjson.list.forEach((d, i) => {
                let tempValue = '';
                let tempValues = []
                keys.forEach(k => {
                    tempValues.push(d[k]);
                })
                tempValue = tempValues.join(',');
                if (!chkHash[tempValue]) {
                    chkHash[tempValue] = 1;
                } else {
                    errLogMgr.addLog([`${td.tableName}${td.plat}: ${keys}: ${tempValue}: 第${i+3}行---主键数据重复`])
                }
            })
        }
    }

}