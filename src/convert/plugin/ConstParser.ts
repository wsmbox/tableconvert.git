import { IPlugIn, ITableObj, ITableData, IConvertParser } from "../../inters/InterfaceApi";
import { IKeyVo, KeyVoUtil } from "../utils/KeyVoUtil";
import { HashMap } from "../utils/HashMap";
import { ValueUtil } from "../utils/ValueUtil";
import { Convert } from "../Convert";
import { convertMgr } from "../mgr/ConvertMgr";
import { errLogMgr } from "../mgr/ErrLogMgr";
import { codeMgr } from "../mgr/CodeMgr";
import { ICls, IInterface } from "../utils/ClsFactory";
import { TypeUtil } from "../utils/TypeUtil";

export class ConstParser implements IPlugIn, IConvertParser {

    private _errLogs: string[];
    protected _hash: HashMap<any>;
    protected _resultData: any;
    protected _clsList: Array<ICls>;
    private _data:ITableObj;
    convert: Convert;

    start(data: any, options?: any, callback?: (res?: any) => void) {
        this._data=data;
        return this;
    }

    parseValue(){
        this.convert = convertMgr.crtConvert;
        let tableData: ITableObj = this._data;
        let keys = tableData.data[0];
        let descs = tableData.data[1];
        let values = tableData.data.slice(2);
        this._clsList = [];
        let tp = this.convert.convertCfg.tsOption.tablePrefix
        var iInter = new IInterface(tp + tableData.fileName);
        this._clsList.push(iInter);
        let keyVos: IKeyVo[] = []
        keys.forEach((keyStr, idx) => {
            let keyVo = KeyVoUtil.parserKey(keyStr, tableData.fileName);
            keyVo.des = descs[idx];

            keyVos.push(keyVo)
        })

        this._hash = new HashMap();
        this._resultData = {};
        for (let i = 0; i < values.length; i++) {
            var propList = values[i];
            if (propList.length == 0) continue;
            var key: string = "";
            var item: any = {};
            for (let j = 0; j < propList.length; j++) {
                var keyVo = keyVos[j];
                try {
                    var value = ValueUtil.parserValue(propList[j], keyVo);
                } catch (e) {
                    this._errLogs.push(tableData.fileName + ":" + propList[0] + "---" + keyVo.name + "--数据错误:" + e.message);
                }
                if (keyVo.primaryKey) {
                    key = value;
                }
                item[keyVo.name] = value;
            }
            if (key === "") {
                key = i.toString();
            }
            var vKeyVo: IKeyVo;
            if (item.type != "enum") {
                vKeyVo = { name: key, type: item.type || "", des: item.des || "", primaryKey: false };
                iInter.addVar(key, TypeUtil.getType(item.type), vKeyVo.des + item.content.toString().replace(/\n/g, ","));
                this._hash.add(key, item);
                try {
                    this._resultData[key] = ValueUtil.parserValue(item.content, vKeyVo);
                } catch (e) {
                    this._errLogs.push(tableData.fileName + ":" + vKeyVo.name + "----数据错误:" + e.message);
                }
            } else {
                vKeyVo = KeyVoUtil.parserKey(key + ":" + item.type + "\nenum:" + item.content, tableData.fileName);
                vKeyVo.enum.setDes(item.des);
                this._clsList.push(vKeyVo.enum);
            }
        }


        let tempTableData: ITableData = {
            type: "const",
            keys: keyVos,
            tableName: tableData.fileName,
            data: this._resultData,
            errLog: this._errLogs,
            plat: tableData.platName
        }
        this.convert.addTableData(tempTableData, tableData.fileName, tableData.platName);

        errLogMgr.addLog(this._errLogs)
        codeMgr.addCls(this._clsList);
    }

    getData(key: string) {
        return null;
    }

    get order(){return 5;}
}