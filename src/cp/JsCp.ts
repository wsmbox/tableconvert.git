import { cpDir } from "../utils/CpFile";
import { logout } from "../utils/LogUtil";

export function fileCopy(src: string, dist: string, opt: { hash?: boolean, compress?: boolean, noMap?: boolean }) {
    let filterFile = opt.noMap ? ['.js.map'] : [];
    cpDir(src, dist, () => {
        logout('==>文件拷贝完毕<==');
    }, opt.hash, { file: filterFile }, opt.compress)
}