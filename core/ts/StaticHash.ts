import { DataHash } from "./DataHash";

/**
* name 
*/
export class StaticHash<T> extends DataHash<T>{
	constructor(d: any, keyName?: string | Array<string>, tableName?: string) {
		super(d, keyName, tableName);
	}

	protected _install(d: any) {
		for (var key in d) {
			this[key] = d[key];
		}
		this._onInstall();
	}
}