import { EventDispatcher } from "./EventDispatcher";

export class DataHash<T> extends EventDispatcher {
    protected _hash: any;
    protected _keyName: string | Array<string>;
    protected _arr: Array<T>;
    protected _primaryKey: string;
    protected _langKeys: Array<string>;
    protected _tableName: string;
    protected _getKey: (d: any) => string;
    static lang: any;
    constructor(d: any, keyName?: string | Array<string>, tableName?: string) {
        super();
        this._keyName = keyName;
        this._hash = {};
        this._arr = [];
        this._tableName = tableName.split(".")[0];
        this._install(d);
    }

    protected _install(d: any) {
        if (!d) {
            console.error(this._tableName + "加载失败");
            return;
        }
        var list = d.list;
        this._primaryKey = d.primaryKey.split(",");
        let keys = this._keyName || this._primaryKey;
        if (typeof keys == "string") {
            this._getKey = this._getSKey;
        } else {
            if (keys.length == 1) {
                this._keyName = keys = keys[0];
                this._getKey = this._getSKey;
            } else {
                this._getKey = this._getAKey;
            }
        }

        this._langKeys = d.langKeys;
        for (var i = 0; list && i < d.list.length; i++) {
            var realKey = this._getKey(list[i]);
            let item = this._createItem(list[i]);
            this._hash[realKey] = item
            this._arr.push(item);
        }
        this._onInstall();
    }

    protected _onInstall() {
        this.event("complete");
    }

    protected _getSKey(d: any) {
        let key: string = <string>(this._keyName || this._primaryKey);
        return d[key];
    }

    protected _getAKey(d: any) {
        let keys = this._keyName || this._primaryKey;
        var realKey = d[keys[0]];
        for (var i = 1; i < keys.length; i++) {
            realKey += "_" + d[keys[i]];
        }
        return realKey;
    }

    protected _createItem(d: any): T {
        for (var i = 0; i < this._langKeys.length; i++) {
            let key = this._getKey(d);
            let langKey = this._tableName + "_" + key + "_" + this._langKeys[i];
            Object.defineProperty(d, this._langKeys[i], {
                get: () => {
                    if (DataHash.lang) {
                        return DataHash.lang[langKey];
                    }
                    return "";
                },
                enumerable: true
            })
        }
        return d;
    }

    public getData(key: string | number, ...arg): T {
        if (!arg || arg.length == 0) {
            return this._hash[key];
        }
        var realKey = key;
        for (var i = 0; i < arg.length; i++) {
            realKey += "_" + arg[i];
        }
        return this._hash[realKey];
    }

    public getAllRes() {
        return this._hash;
    }

    getArr() {
        return this._arr;
    }

    get length() {
        return this._arr.length;
    }

    protected toFixNum(num: number, fix: number = 10): number {
        var numStr = Math.round(num).toString();
        var numLen = numStr.length;
        if (numStr.indexOf("e+") != -1) {
            var arr = numStr.split("e+");
            numLen = parseInt(arr[1]) + 1;
            numStr = arr[0];
        }
        if (numLen <= fix) return num;
        var fixNum = Math.pow(10, numLen - fix);
        return Math.floor(num / fixNum) * fixNum;
    }
}