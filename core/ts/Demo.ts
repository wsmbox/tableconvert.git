//伪代码

import { ConfigTableData } from "./ConfigTableData";
import { DataHash } from "./DataHash";
import { StaticHash } from "./StaticHash";

//加载到二进制包之后
var res: any;        //这个假设就是加载到的数据
var table = new ConfigTableData(res);
var constTable = new StaticHash(table.getTable("Const.json"));      //解析常量表
var langauge=new StaticHash(table.getTable("Language.json"));       //解析语言表
DataHash.lang=langauge;                                             //在底层挂语言表，其他表包含语言的字段会自动从language读取
var test=new DataHash(table.getTable("Test.json"));         //解析其他的普通表