using System.Collections.Generic;
namespace xxbb {
public interface ISysMainKey { string MainKey { get; } }
/**测试枚举*/
public enum SeEnumTest1{
	CeShi1=0,      //测试1
	NiCaiA=2,      //你猜啊
	BuGaoSuNi=3,      //不告诉你
}


/**测试标记*/
public class SeTagsTest2{
	public static int 	CeShi1 = 1;      //测试1
	public static int 	NiCaiA = 4;      //你猜啊
	public static int 	BuGaoSuNi = 8;      //不告诉你
}


public class SeJsonClsReward {
	/***/
	public string 	id;
	/***/
	public int 	num;
	/***/
	public int 	type;
}


public class SeJsonClsLvLimit {
	/**等级*/
	public int 	lv;
	/**等级对应的数量*/
	public int 	num;
}


public class SeJsonClsProp {
	/***/
	public int 	Atk;
	/***/
	public int 	Def;
	/***/
	public int 	Hp;
}


public class SeJsonClsRateReward {
	/***/
	public string 	id;
	/***/
	public int 	num;
	/***/
	public int 	rate;
	/***/
	public SeEnumTest1 	type;
}


public class SeResConst {
	/**兔子单次跳跃高度470*/
	public int 	JumpRange;
	/**关卡开启默认消耗体力3*/
	public int 	expendPower;
	/**奖励{"id":"I001","num":10}*/
	public SeJsonClsReward 	bossReward;
	/**奖励[{"id":"I001","num":10},{"id":"I002","num":2}]*/
	public List<SeJsonClsReward> 	bossRewards;
	/**视频\/分享免费领取体力值12*/
	public int 	vedioFreePower;
	/**引导必中的物品idG004*/
	public string 	GuideGachaponId;
	/**分别是前xx层没道具，道具间隔xx层，后xx层没有道具15,20,20*/
	public List<int> 	DecorateDistance;
}


public enum SeEnumTesttype{
	XueXi=1,      //学习
	XieXie=2,      //谢谢
}


public enum SeEnumTesttypes{
	NiLai=1,      //你来
	WuLi=2,      //物理
}


public class SeResTest : ISysMainKey {
	/**唯一id*/
	public int 	index;
	/**地图皮肤*/
	public string 	map;
	/**总楼层数*/
	public int 	total;
	/**楼层组的id列表*/
	public List<string> 	groups;
	/**首通奖励*/
	public List<string> 	firstReward;
	/**测试json数据*/
	public SeJsonClsReward 	obj;
	/**测试新的列表结构*/
	public List<int> 	nums;
	/**测试枚举*/
	public SeEnumTesttype 	type;
	/**测试枚举列表*/
	public List<SeEnumTesttypes> 	types;
	/**测试布尔类型*/
	public bool 	happy;
	/**测试enum*/
	public SeEnumTest1 	testType;
	/**测试属性*/
	public SeJsonClsProp 	testJsonCls;
	/**测试概率奖励，用[]包裹内容，且用|隔开*/
	public List<SeJsonClsRateReward> 	testRewards;
	public string MainKey{get=>index.ToString();}
}



}