/**测试枚举*/
enum SeEnumTest1{
	CeShi1=0,      //测试1
	NiCaiA=2,      //你猜啊
	BuGaoSuNi=3,      //不告诉你
}

/**测试标记*/
const SeTagsTest2={
	CeShi1:1,      //测试1
	NiCaiA:4,      //你猜啊
	BuGaoSuNi:8,      //不告诉你
}

interface SeJsonClsReward {
	/***/
	id?:string;
	/***/
	num?:number;
	/***/
	type?:number;
}

interface SeJsonClsLvLimit {
	/**等级*/
	lv?:number;
	/**等级对应的数量*/
	num?:number;
}

interface SeJsonClsProp {
	/***/
	Atk?:number;
	/***/
	Def?:number;
	/***/
	Hp?:number;
}

interface SeJsonClsRateReward {
	/***/
	id?:string;
	/***/
	num?:number;
	/***/
	rate?:number;
	/***/
	type?:SeEnumTest1;
}

interface SeResConst {
	/**兔子单次跳跃高度470*/
	JumpRange?:number;
	/**关卡开启默认消耗体力3*/
	expendPower?:number;
	/**奖励{"id":"I001","num":10}*/
	bossReward?:SeJsonClsReward;
	/**奖励[{"id":"I001","num":10},{"id":"I002","num":2}]*/
	bossRewards?:SeJsonClsReward[];
	/**视频\/分享免费领取体力值12*/
	vedioFreePower?:number;
	/**引导必中的物品idG004*/
	GuideGachaponId?:string;
	/**分别是前xx层没道具，道具间隔xx层，后xx层没有道具15,20,20*/
	DecorateDistance?:number[];
}

enum SeEnumTesttype{
	XueXi=1,      //学习
	XieXie=2,      //谢谢
}

enum SeEnumTesttypes{
	NiLai=1,      //你来
	WuLi=2,      //物理
}

interface SeResTest {
	/**唯一id*/
	index?:number;
	/**地图皮肤*/
	map?:string;
	/**总楼层数*/
	total?:number;
	/**楼层组的id列表*/
	groups?:string[];
	/**首通奖励*/
	firstReward?:string[];
	/**测试json数据*/
	obj?:SeJsonClsReward;
	/**测试新的列表结构*/
	nums?:number[];
	/**测试枚举*/
	type?:SeEnumTesttype;
	/**测试枚举列表*/
	types?:Array<SeEnumTesttypes>;
	/**测试布尔类型*/
	happy?:boolean;
	/**测试enum*/
	testType?:SeEnumTest1;
	/**测试属性*/
	testJsonCls?:SeJsonClsProp;
	/**测试概率奖励，用[]包裹内容，且用|隔开*/
	testRewards?:SeJsonClsRateReward[];
}


export {SeEnumTest1,SeTagsTest2,SeJsonClsReward,SeJsonClsLvLimit,SeJsonClsProp,SeJsonClsRateReward,SeResConst,SeEnumTesttype,SeEnumTesttypes,SeResTest}