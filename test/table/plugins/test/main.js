"use strict";
//FYI: https://github.com/Tencent/puerts/blob/master/doc/unity/manual.md
Object.defineProperty(exports, "__esModule", { value: true });
const CSharpWirter_1 = require("./CSharpWirter");
const TestParser_1 = require("./TestParser");
function OnStart(handler) {
    console.log("开始发布");
    handler.registTypeParser("Test4", new TestParser_1.TestParser());
}
exports.OnStart = OnStart;
function OnComplete(handler) {
    // return new CSharpWirter(handler);
    (new CSharpWirter_1.CSharpWirter(handler)).start();
    console.log("发布结束");
}
exports.OnComplete = OnComplete;
