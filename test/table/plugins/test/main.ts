//FYI: https://github.com/Tencent/puerts/blob/master/doc/unity/manual.md

import { CSharpWirter } from "./CSharpWirter";
import { TestParser } from "./TestParser";


function OnStart(handler: Convert) {
    console.log("开始发布");
    handler.registTypeParser("Test4",new TestParser());
}

function OnComplete(handler: Convert) {
    // return new CSharpWirter(handler);
    (new CSharpWirter(handler)).start();
    console.log("发布结束");
}
export { OnStart,OnComplete };