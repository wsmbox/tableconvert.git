"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TestParser {
    parseValue(value) {
        if (!value)
            return [];
        let arr = value.split(":");
        arr.map((v) => parseInt(v) * 100);
        return arr;
    }
    get typeName() { return "number[]"; }
}
exports.TestParser = TestParser;
