export class TestParser implements ITypeParser {
    parseValue(value: string): any {
        if(!value)return [];
        let arr=value.split(":");
        arr.map((v)=>parseInt(v)*100);
        return arr;
    }

    get typeName(): string { return "number[]"; }
}