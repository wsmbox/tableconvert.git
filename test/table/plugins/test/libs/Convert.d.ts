declare interface IKeyVo {
    name: string;
    type: string;
    primaryKey: boolean;
    content?: any;
    des?: string;
    enum?: IEnum;
    tags?: ITags;
    enumkey?: IConst;
}

declare interface ITypeParser {
    parseValue(value: string): any;
    typeName: string;
}
declare class Convert {
    convertCfg: ITableConfig;
    /** 转换后的数据，被各种插件转换后的数据 */
    tableData: {
        [tableName: string]: ITableData;
    };
    /** 从xlsx转出来的源数据 */
    tableSrc: {
        [tableName: string]: ITableObj;
    };
    /** 多个平台的列表，可以用来判断不同的平台使用 */
    plats: string[];
    private _convertParser;
    protected _tablecRootDir: string;
    getTableData(tableName: string, platName?: string): ITableData;
    getParser(tableName: string, platName?: string): IConvertParser;
    registTypeParser(name: string, parser: ITypeParser);
    private _getRealFileName;
}

declare interface IConvertParser {
    getData(key: string): any;
}

declare interface ITableConfig {
    /** 导入配置 */
    input?: {
        /** 导入表的目录,不填则是使用和当前配置同级目录 */
        path?: string;
        /** 导出时，指定前缀的表名才能导出，和 filterPrefix 互斥; eg: 可以将开头为sh的tab都导出 */
        exportPrefix: string;
    };
    /** 导出配置 */
    output: ITableOutput[];
    /** 如果是dts的导出，这里定义导出的配置 */
    tsOption?: {
        /** 导出的是表 前缀这里加上 */
        tablePrefix: string;
        /** 导出的是类型 前缀这里加上 */
        typePrefix: string;
    };
    /** 如果有language，这里设置font的导出目录 */
    fontPath?: string;
    /** 错误日志设置 */
    errorLog?: {
        /** 是否开启数据查错功能（内置插件） */
        check: boolean;
        /** 错误日志输出路径 */
        outputPath: string;
    };
    /** 插件设置，为空则不使用外部插件 */
    plugin?: {
        /** 外部插件的主目录 */
        path: string;
        /** 插件调用列表 */
        list: string[];
    };
}

declare interface ITableOutput {
    /** 导出的代码类型，js; json; d.ts; ... */
    type: string;
    /** 当前导出的对应type的文件路径 */
    path?: string | string[];
    /** 数据拆分（为了减少导出数据量）: 0为不拆分; 1为title和数据拆分; ... */
    dataSplit?: number;
    /** 当前导出的文件需要合成设置，为空则不合并 */
    merge?: IFileMerge[];
    /** 如果是js导出,这里定义的是注册到window上的代码 */
    windowRegister?: string;
    /** 如果是ts，或者dts，这里按照配置名字来导出，默认是sdata */
    fileName?: string;
}

declare interface IFileMerge {
    /** 需要合并的文件列表,为空则在其他的列表不存在的情况下默认选则所有的文件 */
    files?: string[];
    /** 合并是否压缩: 0为直接合并为一个文件; 1为合并文件后进行zip压缩 */
    compress: number;
    /** 合并之后的文件路径 */
    path: string;
    /** 合并之后的文件名，如果有平台区分，则会形成xxx_tt的形式。如果有lang和平台同时存在，变成xx_tt_cn形式 */
    name?: string;
    /** 合并之后的文件扩展名，例如: ".bin", ".config" */
    nameExt?: string;
    /** 分支拆分模式；0为所有文件都合并为一个文件(xx.bin)；
     * 1为将所有分支都分离成各自的不重名表文件(xx_tt.bin, xx_tt_en.bin) */
    branchSplit?: number;
}

declare interface ITableData {
    type: string;
    keys: IKeyVo[];
    tableName: string;
    /** 这里有可能是ITableJson，或者是Const的Obj，或者是Language的Obj */
    data: any;
    /** 转表中错误的报错内容 */
    errLog?: string[];
    /** 如果有平台信息 */
    plat?: string;
}

/** 从xlsx中读取出来的表格数据（原始数据） */
declare interface ITableObj {
    fileName: string;
    tabName: string;
    data: string[][];
    platName: string;
}


declare class IEnum implements ICls {
    private _name;
    private _varList;
    private _hash;
    private _des;
    constructor(name: string);
    setDes(des: string): void;
    addVar(name: string, value: number, des: string): void;
    getValue(name: string): any;
    toString(tabNum?: number): string;
    get name(): string;
}

declare interface ICls {
    toString(tabNum?: number): any;
    name: string;
}

declare class ITags implements ICls {
    private _name;
    private _varList;
    private _hash;
    private _des;
    constructor(name: string);
    setDes(des: string): void;
    addVar(name: string, value: number, des: string): void;
    getValue(name: string): any;
    toString(tabNum?: number): string;
    get name(): string;
}

declare class IConst implements ICls {
    private _name;
    private _varList;
    private _hash;
    private _des;
    constructor(name: string);
    setDes(des: string): void;
    addVar(name: string, value: string, des: string): void;
    getValue(name: string): any;
    toString(tabNum?: number): string;
    get name(): string;
}