"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CSharpWirter {
    constructor(conver) {
        this._convert = conver;
    }
    start() {
        let dtsList = this._convert.getCodeMgr().dtsList;
        for (var tableName in this._convert.tableData) {
            let table = this._convert.tableData[tableName];
            console.log("导出表" + tableName);
        }
    }
}
exports.CSharpWirter = CSharpWirter;
