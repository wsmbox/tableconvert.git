import { join,dirname } from "path";
import { JsonReader } from "../../src/convert/reader/JsonReader"
import { convert } from "../../src/Main";

describe("开始转表", function () {
    it("转表", function (done) {
        let path = join('D:/work/gitee/tools/tableconvert/test/table/tablec.json');
        let headPath = dirname(path);
        new JsonReader().read(path, null, (res) => {
            convert(res, headPath);
        });
        done();
    })
});