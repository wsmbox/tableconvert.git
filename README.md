# xlsxconvert

#### 介绍
将文档表格转换为自定义格式,目前只支持xlsx

#### 用法

1. 初始化配置

进入你要转表的目录，然后在此目录打开命令行工具。

输入 `tablec init`，可以生成最基本的配置

2. 转表

用法1:

进入 tablec.json 所在的目录，然后在此目录打开命令行工具。

输入 `tablec c`，即可开始转表

用法2:

随意目录打开命令行工具

输入 `tablec c=D:/static/table/tablec.json`


#### 转表配置

tablec.json

``` json
{
    "input": {
        "path": "",// 如果这里填了,则是输入表的路径
        "exportPrefix": "sh"// 需要导出的表明的前缀,即:sh的前缀的都导出
    },
    "output": [
        {
            "type": "json",
            "path": [
                "../output/json"/**导出json的路径，可以是字符串也可以是列表，为空则不导出json文件*/
            ],
            "dataSplit": 0,// (未实现)合并数据的方式,默认0; 1为数据和title分离
            "merge": [
                {
                    "files": [],//(未实现,先填空)文件分开组合
                    "compress": 1,//是否压缩
                    "path": "../output/json/allinone",//合并后的文件路径
                    "name": "data",//合并后的文件名
                    "nameExt": ".config",//合并后的文件扩展名
                    "branchSplit": 1// 是否要将不同的平台分开合并
                }
            ]
        },
        {
            "type": "ts",
            "path": [
                "../output/ts"/**导出ts的路径，可以是字符串也可以是列表*/
            ],
            "fileName": "sdata"/** 导出的ts的名字，不填默认为sdata*/
        },
        {
            "type": "cs",
            "path": "../output/cs",/**导出cs的路径，可以是字符串也可以是列表*/
            "fileName": "SysTableData",/** 导出的ts的名字，必须填*/
            "fileStart": "using System.Collections.Generic;\nnamespace xx {\n",/** 导出文件时，在文件头加的命名空间包裹 */
            "fileEnd": "\n}"
        }
    ],
    "tsOption": { /**导出ts时的设置，不设置则默认为以下设置*/
        "tablePrefix": "SeRes",/**ts导出的接口或类的前缀,一般后面是表名*/
        "typePrefix": "Se"/**ts导出类型的前缀,一般后面是Enum,Tags等...*/
    },
    "fontPath": "../output/font",/**文字去重后的导出txt文本路径,不设置则不导出font文件*/
    "errorLog": {/**如果检查出错的导出log，不设置则不导出log*/
        "check": true, /**是否检查错误*/
        "outputPath": "../output/errlog"/**错误输出路径,未输出则无错误*/
    },
    "plugin": {/** 使用本地自定义插件的设置，不设置则只会运行默认插件目录和自动插件目录，自动目录为json文件同级的plugins目录*/
        "path": "" /**指定插件的路径*/
    }
}

```

插件demo[地址](https://gitee.com/sleepfisher/tableconvertjs-plugins-demo)

#### 表的规则

1. 表格的第一行为:表头配置，使用alt+Enter可以在一行中，分为两行，这时，第二行可以使用附加内容

eg：primaryKey:true(表示是主键，每个表都需要主键)；enum:枚举1|1|枚举2|2(表示枚举内容)

2. 表格的第二行为:表头的注释

3. 从第三行开始,为表的具体内容数据，如果是枚举或tags类型，填写枚举的中文（枚举列表用|分割开的0，2，4...的index位置的字符串）

如果是`string[],Array<string>,number[]`之类的，则可以用`,`隔开

4. 如果是准备用于导出到cs的表，注意number需要改成int和float，支持int和float类型

5. 增加了外部的全局Enum定义，在Enum表中定义一次，然后可以在其他的表里随意用定义好的名字

6. 增加了外部的全局Json定义，在Json表中定义一次，然后其他的表里可以使用，包括列表

7. 增加全局JsonCls定义，其他表里可以以：`id:xx,num:2` 的形式编辑，值类型目前只支持：int，float，number，string，以及其对应的列表

#### 插件的规则

外部插件只需要实现start方法即可,为js文件
``` js
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Hello = void 0;
var Hello = /** @class */ (function () {
    function Hello(args) {
        this._convert = args;// 代码内调用时,默认会将转表类传入，其中缓存了所有表的源数据和转完后的数据集
    }
    Hello.prototype.start = function (data, callback) {
        console.log('hello',this._convert.convertCfg)// 这里拿到的是转表的配置
    };
    return Hello;
}());
exports.PlugIn = Hello; // 这里注意导出的名字是PlugIn

```

### 解析代码
查看代码包的core目录下
